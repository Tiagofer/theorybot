# TheoryBot

### A Discord Bot that helps you learn music theory.

Made by Tiagofer for Lis_13

## Installation Instructions

1. Make your own [Discord Bot](https://discord.com/developers/applications) and add it to your Discord server. There are loads of tutorials on this, look one up. Required permissions are View Channels, Send Messages, Manage Messages, Embed Links, Attach Files, Read Message History, Use External Emojis, and Add Reactions.
1. Install [NodeJS and NPM](https://nodejs.org/) on your System.
1. Clone the repo to a folder of your choice.
1. Make a config.js file (an example one is provided).
1. Copy your Discord Bot Token to the respective field on the config.js file.
1. Make a new folder and place your images there. Update the config file with the path to it and the image names. 
1. On a terminal, navigate to the folder you cloned the repo to and install the node packages with `npm i`.
1. Run `node run start` to run the bot. Quit it with `Ctrl-C`.

